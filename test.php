<?php
require 'vendor/autoload.php';

use ND\ShipmentsApi\Credentials;
use ND\ShipmentsApi\ShipmentsApi;

$s = new ShipmentsApi(new Credentials('pood@nordic-digital.ee', 'secret','token5.dat'));
$s->setTesting(false);
$locations = $s->getLocations('OmnivaPostOffice');
print_r($locations);
exit;
//Kuller

$params = new \ND\ShipmentsApi\Shipment\Params(
    'Andres Kahar',
        'Test 4',
        'Tõrvandi',
        '+37253798421',
        'pood@nordic-digital.ee',
        '61715',
        'EE',
        'DPDCourier',
        1,
        null,
        'PEO2345'
);

//Pakiautomaat


$params = new \ND\ShipmentsApi\Shipment\Params(
    'Andres Kahar',
    null,
    null,
    '+37253798421',
    'andres.kahar@nordic-digital.ee',
    null,
    'EE',
    'VenipakPickup',
    1,
    '3350',
    'PEO2345'
);

/*$params = new \ND\ShipmentsApi\Shipment\Params(
    'Andres Kahar',
    null,
    null,
    '+37253798421',
    'andres.kahar@nordic-digital.ee',
    null,
    'EE',
    'DPDPickup',
    1,
    'EE90019',
    'PEO2345'
);*/

$response = $s->shipment()->create($params);
if($response->success()) {
    echo "Saadetis koostatud!\n\r";
    print_r($response->barcodes());
} else {
    echo "Saadetise koostamine ebaõnnestus! " . $response->error() . "\n\r";
}
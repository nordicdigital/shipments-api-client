<?php


namespace ND\ShipmentsApi;


/**
 * Class Credentials
 * @package ND\ShipmentsApi
 */
class Credentials
{
    /**
     * Username
     *
     * @var string
     */
    private $username;

    /**
     * @return mixed
     */
    public function getTokenFilePath()
    {
        return $this->tokenFilePath;
    }

    /**
     * @param mixed $tokenFilePath
     */
    public function setTokenFilePath($tokenFilePath)
    {
        $this->tokenFilePath = $tokenFilePath;
    }

    /**
     * Password
     *
     * @var string
     */
    private $password;

    /**
     * @var
     */
    private $tokenFilePath;

    /**
     * Main construct method
     *
     * @param string $username
     * @param string $password
     */
    public function __construct(string $username, string $password, string $tokenFilePath)
    {
        $this->username = $username;
        $this->password = $password;
        $this->tokenFilePath = $tokenFilePath;
    }

    /**
     * Get username
     *
     * @return  string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set username
     *
     * @param  string  $username  Username
     *
     * @return  Credentials
     */
    public function setUsername(string $username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get password
     *
     * @return  string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set password
     *
     * @param  string  $password  Password
     *
     * @return  self
     */
    public function setPassword(string $password)
    {
        $this->password = $password;

        return $this;
    }
}
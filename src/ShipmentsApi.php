<?php
namespace ND\ShipmentsApi;


use ND\ShipmentsApi\Credentials;
use ND\ShipmentsApi\Shipment\Shipment;

class ShipmentsApi
{

    protected $testing;

    /**
     * @return mixed
     */
    public function getTesting()
    {
        return $this->testing;
    }

    /**
     * @param mixed $testing
     */
    public function setTesting($testing)
    {
        $this->testing = $testing;
    }

    public function __construct(Credentials $credentials = null, bool $testing = null)
    {
        if (!$credentials) {
            throw new \Exception('Credentials not found. Please pass credentials.');
        }
        $this->credentials = $credentials;
        $this->setTesting((bool)$testing);
        $this->httpClient = new HttpClient([
            'verify' => false,
            'base_uri' => $this->getServerURL()
        ]);

        $token = $this->auth();
        $this->httpClient = new HttpClient([
            'verify' => false,
            'base_uri' => $this->getServerURL(),
            'http_errors' => false,
            'headers' => [
                'Authorization' => 'Bearer ' . $token
            ]
        ]);

    }

    protected function auth()
    {
        if(
            file_exists($this->credentials->getTokenFilePath())
            && filemtime($this->credentials->getTokenFilePath()) > time() - 30 * 24 * 60 * 60
        ) {
            return file_get_contents($this->credentials->getTokenFilePath());
        }

        $response = $this->httpClient->post('login',  ['form_params' =>
            [
                'email' => $this->credentials->getUsername(),
                'password' => $this->credentials->getPassword(),
            ]
        ]);
        if($response->getStatusCode() != 200) {
            throw new \Exception('Login failed!');
        }
        $responseBody = (string)$response->getBody();
        $responseData = json_decode($responseBody, true);

        if(
            !isset($responseData['token'])
            || empty($responseData['token'])
        ) {
            throw new \Exception('Login failed! Empty or missing token!');
        }
        file_put_contents($this->credentials->getTokenFilePath(), $responseData['token']);

        return $responseData['token'];
    }

    public function getServerURL()
    {
        if($this->getTesting()) {
            return 'http://saadetised-dev.nordic-digital.ee:8101/api/';
        }
        return 'https://saadetised.nordic-digital.ee/api/';
    }

    public function getLocations(string $carrier)
    {
        $response = $this->httpClient->get('carrier/locations/' . $carrier);
        if($response->getStatusCode() != 200) {
            throw new \Exception('Locations not found!');
        }
        $responseBody = (string)$response->getBody();
        return $responseBody;
    }

    public function getShipmentStatus(string $trackingCode)
    {
        $response = $this->httpClient->get('shipment-status/' . $trackingCode);
        if($response->getStatusCode() != 200) {
            throw new \Exception('Shipment status not found!');
        }
        $responseBody = (string)$response->getBody();
        return $responseBody;
    }

    public function shipment()
    {
        return new Shipment($this->httpClient);
    }

}
<?php


namespace ND\ShipmentsApi\Shipment;


use ND\ShipmentsApi\BaseResponse;

/**
 * Response method returns barcode info
 */
class Response extends BaseResponse
{
    /**
     * Tracking code, Barcode
     *
     * @var string
     */
    private $barcode;

    /**
     * Returns if response was success
     *
     * @return boolean
     */
    public function success()
    {
        if (
            !isset($this->response->status)
            || $this->response->status !== 'ok'
        ) {
            return false;
        }

        return true;
    }

    /**
     * Returns barcode
     *
     * @return string|null
     */
    public function barcode()
    {
        if (!$this->success()) {
            return null;
        }

        if (!isset($this->response->barcodes[0])) {
            return null;
        }

        $this->barcode = $this->response->barcodes[0];
        return $this->barcode;
    }

    public function barcodes()
    {
        if (!$this->success()) {
            return null;
        }

        if (!isset($this->response->barcodes[0])) {
            return null;
        }

        $barCodes = [];
        foreach($this->response->barcodes as $barcode) {
            $barCodes[] = $barcode;
        }
        return $barCodes;
    }

    public function error()
    {
        if (!isset($this->response->error)) {
            return null;
        }
        return $this->response->error;
    }
}
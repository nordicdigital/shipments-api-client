<?php

namespace ND\ShipmentsApi\Shipment;

use ND\ShipmentsApi\HttpClientInterface;

class Location
{

    /**
     * Http Client
     *
     * @var HttpClientInterface
     *
     */
    protected $httpClient;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function get()
    {
        $response = $this->httpClient->request(
            'GET',
            'shipment/locations'
        );

        return new Response((string) $response->getBody());
    }
}
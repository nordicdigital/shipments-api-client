<?php


namespace ND\ShipmentsApi\Shipment;


class Carrier
{

    const CARRIER_DPD_COURIER = 'DPDCourier';
    const CARRIER_DPD_PICKUP = 'DPDPickup';
    const CARRIER_SMARTPOST_ESTONIA_PARCEL = 'SmartpostParcelTerminalEstonia';
    const CARRIER_VENIPAK_COURIER = 'Venipak';
    const CARRIER_VENIPAK_PICKUP = 'VenipakPickup';
    const CARRIER_OMNIVA_PARCEL_TERMINAL = 'OmnivaParcelTerminal';
    const CARRIER_OMNIVA_MATKAHUOLTO = 'OmnivaMatkahuolto';
    const CARRIER_OMNIVA_POST_OFFICE = 'OmnivaPostOffice';

    const ALLOWED_CARRIERS = [
        self::CARRIER_DPD_COURIER,
        self::CARRIER_DPD_PICKUP,
        self::CARRIER_SMARTPOST_ESTONIA_PARCEL,
        self::CARRIER_VENIPAK_COURIER,
        self::CARRIER_VENIPAK_PICKUP,
        self::CARRIER_OMNIVA_MATKAHUOLTO,
        self::CARRIER_OMNIVA_PARCEL_TERMINAL,
        self::CARRIER_OMNIVA_POST_OFFICE
    ];

    public static function isKnownCarrier($carrier)
    {
        return in_array($carrier, self::ALLOWED_CARRIERS);
    }

}
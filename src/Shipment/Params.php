<?php


namespace ND\ShipmentsApi\Shipment;


/**
 * Class Params
 * @package ND\ShipmentsApi\Shipment
 */
class Params
{


    /**
     * @var
     */
    private $address;
    /**
     * @var
     */
    private $carrier;
    /**
     * @var
     */
    private $city;

    /**
     * @var
     */
    private $comment;


    /**
     * @var
     */
    private $country;

    /**
     * @var
     */
    private $email;
    /**
     * @var
     */
    private $name;
    /**
     * @var
     */
    private $packageCount;
    /**
     * @var
     */
    private $parcelShopId;
    /**
     * @var
     */
    private $phone;


    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * @param mixed $carrier
     */
    public function setCarrier($carrier)
    {
        if (!Carrier::isKnownCarrier($carrier)) {
            throw new \Exception('Unknown carrier!');
        }
        $this->carrier = $carrier;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPackageCount()
    {
        return $this->packageCount;
    }

    /**
     * @param mixed $packageCount
     */
    public function setPackageCount($packageCount)
    {
        $this->packageCount = $packageCount;
    }

    /**
     * @return mixed
     */
    public function getParcelShopId()
    {
        return $this->parcelShopId;
    }

    /**
     * @param mixed $parcelShopId
     */
    public function setParcelShopId($parcelShopId)
    {
        $this->parcelShopId = $parcelShopId;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param mixed $postalCode
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @var
     */
    private $postalCode;


    /**
     * Params constructor.
     * @param $name
     * @param $address
     * @param $city
     * @param $phone
     * @param $postalCode
     * @param $country
     * @param $carrier
     * @param int $packageCount
     * @param null $parcelShopId
     */
    public function __construct(
        $name,
        $address,
        $city,
        $phone,
        $email,
        $postalCode,
        $country,
        $carrier,
        $packageCount = 1,
        $parcelShopId = null,
        $comment = null
    )
    {
        $this->setName($name);
        $this->setAddress($address);
        $this->setCity($city);
        $this->setPhone($phone);
        $this->setEmail($email);
        $this->setPostalCode($postalCode);
        $this->setCountry($country);
        $this->setCarrier($carrier);
        $this->setPackageCount($packageCount);
        $this->setParcelShopId($parcelShopId);
        $this->setComment($comment);
    }
}
<?php


namespace ND\ShipmentsApi\Shipment;

use ND\ShipmentsApi\HttpClientInterface;
use ND\ShipmentsApi\Shipment\Params;

class Create
{
    /**
     * Http Client
     *
     * @var HttpClient
     */
    protected $httpClien;

    /**
     * Params
     *
     * @var Params
     */
    protected $params;

    public function __construct(HttpClientInterface $httpClient, Params $params)
    {
        $this->httpClient = $httpClient;
        $this->params = $params;
    }

    /**
     * Runs create
     *
     * @return void
     */
    public function run()
    {

        $postData = [
            'receiverName' => $this->params->getName(),
            'receiverAddress' => $this->params->getAddress(),
            'receiverPhone' => $this->params->getPhone(),
            'receiverCity' => $this->params->getCity(),
            'receiverPostcode' => $this->params->getPostalCode(),
            'receiverEmail' => $this->params->getEmail(),
            'receiverCountry' => $this->params->getCountry(),
            'packageComment' => $this->params->getComment(),
            'packageCount' => $this->params->getPackageCount(),
            'carrier' => $this->params->getCarrier(),
            'parcelShopId' => $this->params->getParcelShopId()
        ];

        $response = $this->httpClient->request(
            'POST',
            'shipment/create',
            [
                'form_params' => $postData
            ]);

        return new Response((string) $response->getBody());
    }
}
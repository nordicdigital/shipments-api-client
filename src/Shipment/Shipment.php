<?php


namespace ND\ShipmentsApi\Shipment;


use ND\ShipmentsApi\Shipment\Create;
use ND\ShipmentsApi\HttpClientInterface;

class Shipment
{

    /**
     * Shipment Params
     *
     * @var Params
     */
    protected $params;

    /**
     * Create shipment
     *
     * @var Create
     */
    protected $create;

    /**
     * Http Client
     *
     * @var HttpClientInterface
     *
     */
    protected $httpClient;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * Creates shipment
     *
     * @return Response
     */
    public function create(Params $params)
    {
        $this->params = $params;
        $create = new Create($this->httpClient, $this->params);
        return $create->run();
    }

    /**
     * Returns shipment params
     *
     * @return Params
     */
    public function params()
    {
        return $this->params;
    }
}
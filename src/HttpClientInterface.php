<?php


namespace ND\ShipmentsApi;


interface HttpClientInterface
{

    /**
     * Request method
     *
     * @param string $method
     * @param string $url
     * @param array $params
     * @return void
     */
    public function request(string $method, string $url, array $params);
}
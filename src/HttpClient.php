<?php


namespace ND\ShipmentsApi;

use GuzzleHttp\Client as GuzzleClient;

/**
 * Default http client using Guzzle library
 */
class HttpClient extends GuzzleClient implements HttpClientInterface
{

}
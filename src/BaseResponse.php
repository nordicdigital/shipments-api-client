<?php


namespace ND\ShipmentsApi;


abstract class BaseResponse
{
    /**
     * Response object
     *
     * @var object
     */
    protected $response;

    public function __construct(string $response)
    {
        $this->response = json_decode($response);
    }

    /**
     * Returns raw response
     *
     * @return object
     */
    public function raw()
    {
        return $this->response;
    }
}